class EasyMoneyAccountForm extends Polymer.Element {

  static get is() {
    return 'easy-money-account-form';
  }

  static get properties() {
    return {
      currency: {
        type: String,
        value: "EUR"
      },
      conditions: {
        type: Array,
        value:[
          {
            "title": "CONDICIONES",
            "items": [{
                "key": "Periodicidad de liquidación",
                "isAmount": false,
                "value": "SEMESTRAL"
              },
              {
                "key": "Tipo de Interés Acreedor",
                "isAmount": true,
                "value": {
                  "amount": 0,
                  "currency": "EUR"
                }
              },
              {
                  "key": "Comisión de Administración",
                  "isAmount": true,
                  "value": {
                    "amount": 0,
                    "currency": "EUR"
                  }
            }]
          }
        ]
      },
      type: {
        type: String,
        value: "Cuenta Personal"
      },
      user:{
        type: Object
      },
      account:{
        type: Object
      },
      loading:{
        type: Boolean,
        notify: true,
        value: false
      }
    };
  }

  _handleCancelarClick(evt){
    this.dispatchEvent(new CustomEvent('account-form-cancel',{bubbles:true, composed:true}));
  }
  _handleContratarClick(evt){
    this.loading = true;
    const normalizedAccount = {
      user_id: this.user.id,
      currency : this.currency,
      conditions: this.conditions,
      type: this.type
    };
    this.$.addaccountdp.headers = {
      authorization : "JWT " + sessionStorage.getItem("token"),
      user_id : parseInt(sessionStorage.getItem("userId"))
    };
    this.$.addaccountdp.body = normalizedAccount;
    this.$.addaccountdp.host = cells.urlEasyMoneyBankBankServices;
    this.$.addaccountdp.generateRequest();
  }
  _handleRequestAddAccountSuccess(evt) {
    const detail = evt.detail;
    this.loading = false;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.dispatchEvent(new CustomEvent('add-account-ok',
        {bubbles: true, composed: true, detail: { msg: msg }}));
  }
  _handleRequestAddAccountError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('add-account-error',
        {bubbles:true, composed:true,  detail: { msg: msg }}));
  }
}
customElements.define(EasyMoneyAccountForm.is, EasyMoneyAccountForm);
